var MongoClient = require('mongodb').MongoClient;

var getFacebookData = require("./FacebookData.js").get;
var getVotes = require("./Votes.js").get;
var getVoteHistory = require("./VoteHistory.js").get;
var getVoteHistoryDb = require("./VoteHistoryDb.js").get;
var db = require('./Db.js');

var http = require("http");
var url = require('url');
var CronJob = require('cron').CronJob;

var graph = require('fbgraph');
var express = require('express');
var app = express();
var router = express.Router();

var moment = require('moment-timezone');
var fs = require("fs");
var exec = require('child_process').exec;


app.use(express.static('public'));
app.set('view engine', 'jade');

start();


app.get('/aggiorna', function (req, res) {
	aggiorna(res);
});

app.post('/aggiorna', function (req, res) {
	aggiorna(res);
});

function aggiorna(res) {
	console.log('pulling...');
	exec("git pull origin master", function(error, stdout, stderr) {
		if (!error) {
			res.send('ok');
			console.log('ok');
		} else {
			res.send('oops qualcosa è andato storto');
			console.log('oops qualcosa è andato storto');
			
		}
	});
}



// app.get('/vota', function (req, res) {
// 	res.render('vota');
// });


// app.get('/', function (req, res) {

// 	fs.readFile('./voti.json', 'utf8', function (err, data) {
// 		if (err) throw err;
// 		var obj = JSON.parse(data);

// 		res.render('index', {
// 			data: obj.voti,
// 			date: obj.date
// 		});
// 	});


// });




// app.get('/storico', function (req, res) {
// 	res.render('storico');
// });

// app.get('/facebook', function (req, res) {
// 	res.render('facebookdb');
// });


app.get('/storie2', function (req, response) {

	http.get("http://lascuolaopensource.xyz/proxyform.php", function (res) {
		var body = '';

		res.on('data', function (chunk) {
			body += chunk;
		});

		res.on('end', function () {
			var d = JSON.parse(body);
			response.render('storie', {
				data: d
			});
		});
	}).on('error', function (e) {
		console.log("Got an error: ", e);
	});
});


app.get('/storie', function (req, response) {

	http.get("http://lascuolaopensource.xyz/proxyform.php", function (res) {
		var body = '';

		res.on('data', function (chunk) {
			body += chunk;
		});

		res.on('end', function () {
			var d = JSON.parse(body);
			response.render('storie', {
				data: d
			});
		});
	}).on('error', function (e) {
		console.log("Got an error: ", e);
	});

});

app.get('/facebookregister', function (req, res) {

	// we don't have a code yet
	// so we'll redirect to the oauth dialog
	if (!req.query.code) {
		var authUrl = graph.getOauthUrl({
			"client_id": conf.client_id,
			"redirect_uri": conf.redirect_uri,
			"scope": conf.scope
		});

		if (!req.query.error) { //checks whether a user denied the app facebook login/permissions
			res.redirect(authUrl);
		} else { //req.query.error == 'access_denied'
		res.send('access denied');
	}
	return;
}

	// code is set
	// we'll send that and get the access token
	graph.authorize({
		"client_id": conf.client_id,
		"redirect_uri": conf.redirect_uri,
		"client_secret": conf.client_secret,
		"code": req.query.code
	}, function (err, facebookRes) {
		res.redirect('/UserHasLoggedIn');
	});


});


// user gets sent here after being authorized
app.get('/UserHasLoggedIn', function (req, res) {
	res.render("index", {
		title: "Logged In"
	});
});


//-------API

app.get('/api/storico', function (req, res) {

	if (!req.query.since && !req.query.until) {

		db.get().collection('documents').find({}).toArray(function (err, docs) {
			res.send(docs);
		});

	} else {

		var result = db.get().collection('documents').aggregate([{
			$unwind: "$votes"
		}, {
			$match: {
				"votes.date": {
					$gte: new Date(req.query.since || new Date("2015/09/18")),
					$lte: new Date(req.query.until || new Date()),
				}
			}
		}, {
			$group: {
				_id: "$name",
				votes: {
					$push: "$votes"
				}
			}
		}]);

		result.toArray(function (err, docs) {
			res.send(docs);
		});

	}

	//db.documents.aggregate([ {$unwind: "$votes"}, {$match:{"votes.votes": 499}}, {$group: {_id:"$name", votes: {$push: "$votes"}}}])

});



// TODO store fb data in db
// every two hours update the database and update the facebookdata
function update() {
	getVoteHistoryDb(function () {
		getFacebookData();
	});
}

function start() {
	var server = app.listen(3000, function () {
		console.log('Listening on port 3000...')

		server.address().address;
		server.address().port;

		//every five minutes get votes
		// setInterval(function () {
		// 	console.log("calling cron votes");
		// 	getVotes();
		// }, 300000);
});
}

function connectDb() {

	db.connect('mongodb://localhost:27017/myproject', function (err) {
		if (err) {
			console.log('Unable to connect to Mongo.')
			process.exit(1)
		} else {
			start();
		}
	})
}



