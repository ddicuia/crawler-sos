var fs = require("fs");
var MongoClient = require('mongodb').MongoClient;

function get(callback) {


	console.log("connecting...");

	MongoClient.connect('mongodb://localhost:27017/myproject', function (err, db) {
		if (err) return done(err)

		if (!db) {
			console.log("ERROR - database not connected");
			return;
		}
		var collection = db.collection('documents');

		fs.readFile('./voti.json', 'utf8', function (err, data) {

			if (err) {
				console.log("ERROR - can't read votes");
				return;
			}

			var currentdate = new Date();
			var lastVotes = JSON.parse(data).voti;


			for (var k = 0; k < lastVotes.length; k++) {
				var name = lastVotes[k].t;
				console.log(name, {
					votes: lastVotes[k].v,
					date: currentdate
				});
				collection.update({
					name: name
				}, {
					$push: {
						votes: {
							votes: lastVotes[k].v,
							date: currentdate
						}
					}
				});
			}

			console.log("database updated");
			if (callback) callback();

		});
	});

}

module.exports = {
	get: get
}