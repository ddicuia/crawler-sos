var fs = require("fs");
var MongoClient = require('mongodb').MongoClient;


console.log("connecting...");

MongoClient.connect('mongodb://localhost:27017/myproject', function (err, db) {

	var collection = db.collection('documents');

	var d = collection.find({}).toArray(function (err, docs) {


		var c = 0;
		var previousVotepoint;
		var temp = [];

		for (var i = 0; i < docs.length; i++) {
			// for (var i = 0; i < 1; i++) {

			var gaps = [];
			temp.push({
				name: docs[i].name,
				votes: []
			});
			// find gaps
			for (var k = 0; k < docs[i].votes.length; k++) {
				var votePoint = docs[i].votes[k];
				temp[i].votes.push(votePoint);

				if (previousVotepoint) {
					var dateA = new Date(votePoint.date);
					var dateB = new Date(previousVotepoint.date);
					if (dateA - dateB > 3700000) {
						var hourDifference = dateA.getHours() - dateB.getHours();
						var startHours = parseInt(dateB.getHours());
						var endHours = parseInt(dateA.getHours());
						var startVotes = previousVotepoint.votes;
						var endVotes = votePoint.votes;

						// console.log("found gap");
						// console.log(dateA);
						// console.log(dateB);
						// console.log("hour difference", hourDifference);
						console.log("startVotes", startVotes);
						console.log("endVotes", endVotes);
						console.log("startHours", startHours);
						console.log("endHours", endHours);


						var change = endHours - startHours;
						var changeVotes = endVotes - startVotes;
						for (var c = 1; c < change; c++) {

							var newVotes = Math.floor((c / change) * changeVotes) + startVotes;
							var d = new Date(dateB.setHours(c + startHours))
							temp[i].votes.push({
								date: d,
								votes: newVotes
							});

							console.log(d.getHours() + " - " + newVotes);
							// console.log("----------");
						}


					}
				}

				previousVotepoint = votePoint;
			}

			// for (var n = 0; n < gaps.length; n++) {
			// 	docs[i].votes.push(gaps[n]);
			// }

			temp[i].votes.sort(function (a, b) {
				return new Date(a.date) - new Date(b.date);
			})
		}

		fs.writeFile("./public/storicovoti2.json", JSON.stringify(temp), "utf8", function () {
			console.log("all done");
			process.exit();
		});

	});


	function easeInQuadratic(t, b, c, d) {
		t /= d;
		var ts = t;
		return b + c * (ts);
	};


	//current time, start, change, duration


});