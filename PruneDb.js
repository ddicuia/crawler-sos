// {
// 	id: 123,
// 	name: "test",
// 	votes: [
// 		votes: 35,
// 		date: mydate
// 	]
// }

var MongoClient = require('mongodb').MongoClient;


console.log("connecting...");

MongoClient.connect('mongodb://localhost:27017/myproject', function (err, db) {
	if (err) return done(err)

	if (!db) {
		console.log("ERROR - database not connected");
		return;
	}
	var collection = db.collection('documents');
	var date = new Date("2015-10-04T11:00:00.000Z");

	collection.update({}, {
		$pull: {
			votes: {
				date: {
					"$gt": date
				}
			}
		},

	}, {
		multi: true
	}, function () {
		process.exit()
	});
});