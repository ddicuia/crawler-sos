var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

var fs = require("fs");


// Connection URL
var url = 'mongodb://localhost:27017/myproject';
// Use connect method to connect to the Server
fs.readFile('./public/storicovoti.json', 'utf8', function (err, data) {

	data = JSON.parse(data);

	var toInsert = [];
	for (var i in data) {
		console.log(data[i]);
		var votesToinsert = data[i];
		for (var k = 0; k < votesToinsert.length; k++) {
			votesToinsert[k] = {
				votes: votesToinsert[k].votes,
				date: new Date(votesToinsert[k].date)
			}
		};
		toInsert.push({
			name: i.replace(/\"/g, ""),
			votes: votesToinsert
		});

	}

	MongoClient.connect(url, function (err, db) {
		assert.equal(null, err);
		console.log("Connected correctly to server");

		removeAll(db, function () {
			insertDocuments(db, toInsert, function () {
				findDocuments(db, function () {
					db.close();
				})
			})
		})
	})
});


var removeAll = function (db, callback) {
	// Get the documents collection
	var collection = db.collection('documents');
	// Insert some documents
	collection.remove({}, function (err, result) {
		console.log("Removed all the documents");
		callback(result);
	});
}

var insertDocuments = function (db, toInsert, callback) {
	// Get the documents collection
	var collection = db.collection('documents');
	// Insert some documents
	collection.insert(toInsert, function (err, result) {
		callback(result);
	});
}

var findDocuments = function (db, callback) {
	// Get the documents collection
	var collection = db.collection('documents');
	// Find some documents
	collection.find({}).toArray(function (err, result) {
		// assert.equal(err, null);
		// assert.equal(2, result.length);
		console.log(result.length);
		callback(result);
	});
}