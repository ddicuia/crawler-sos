require("./FacebookGraph.js");

var http = require("http");
var fs = require("fs");
var graph = require('fbgraph');
var moment = require('moment');
var momenttz = require('moment-timezone');


var fields = "created_time,picture,message,shares,comments.limit(1).summary(true),likes.limit(1).summary(true)";

var since = moment(new Date('2015/09/13')).format("X");

var url = "http://spreadsheets.google.com/feeds/list/11nt6jILUmNWkAAvwPt0_NxbaJfWkNippcuto0j8MA7Q/od6/public/values?alt=json";

var counter = 0;
var a = [];
var allData = [];

var data = [{
	name: "I’m Smart",
	id: "1509914375944535"
}, {
	name: "baumhaus",
	id: "933910153347934"
}, {
	name: "Tournée da Bar",
	id: "916911265041034"
}, {
	name: "La Scuola Open Source",
	id: "854506414639006"
}, {
	name: "Teatroxcasa",
	id: "715285418502908"
}, {
	name: "Liminaria",
	id: "664404650293533"
}, {
	name: "Assessorato alle piccole cose",
	id: "640518816076417"
}, {
	name: "Italia che Cambia",
	id: "531818910222372"
}, {
	name: "StreetArt Factory",
	id: "452659814806757"
}, {
	name: "Xanadu",
	id: "447619071914896"
}, {
	name: "MyHomeGallery.org – Esperienze uniche nelle case degli artisti",
	id: "431178446902266"
}, {
	name: "a proposito di altri mondi",
	id: "409443852479107"
}, {
	name: "Città tra le mani",
	id: "396563993739653"
}, {
	name: "We Are Cinema",
	id: "314623571908379"
}, {
	name: "Puglia Off",
	id: "271483833017632"
}, {
	name: "Pigmenti: laboratorio itinerante di arte pubblica e narrazione",
	id: "144939949011718"
}, {
	name: "LA PIANA progetto d’arte partecipata",
	id: "282477709531"
}, {
	name: "Home Movies Digital Archive",
	id: "73348509449"
}];


function init() {
	a = data;
	next();
}


function loadFromSpreadhseet() {
	http.get(url, onDataReceived);
}

function onDataReceived(res) {
	var body = '';

	res.on('data', function (chunk) {
		body += chunk;
	});

	res.on('end', function () {
		var entries = JSON.parse(body).feed.entry;
		for (var i = 0; i < entries.length; i++) {
			var d = entries[i]
			var n = d.gsx$nomevedipaginachefare;
			var id = d.gsx$id;
			if (n.$t && id.$t) {
				a.push({
					name: n.$t,
					id: id.$t
				});
			}

		}

		next();

	});
}


function next() {


	if (counter < a.length - 1) {
		var f = a[counter];
		console.log("getting ", f.name);
		getData(f.name, f.id)
		counter++;

	} else {
		fs.writeFile("./public/facebook.json", JSON.stringify({
			data: allData,
			date: momenttz.tz(new Date(), "Europe/Rome").format("DD:hh:mma")
		}), "utf8", function () {
			console.log("tutto fatto");
		});
	}
}

function getData(name, id) {
	console.log(id);
	var k = "/" + id + "/posts?limit=100&since=" + since + "&fields=" + fields;
	var counter = 0;
	var data = [];
	callFb();

	function callFb() {

		graph.get(k, function (err, res) {

			if (err) {
				console.log("ERROR", err);
				return;
			}

			data = extractData(res.data);


			allData.push({
				feed: data,
				name: name
			});

			next();

		});
	}

}


function extractData(d) {

	var data = [];
	for (var i = 0; i < d.length; i++) {
		data.push({
			id: d[i].id,
			picture: d[i].picture,
			message: d[i].message,
			created_time: d[i].created_time,
			shares: d[i].shares ? d[i].shares.count : 0,
			likes: d[i].likes ? d[i].likes.summary.total_count : 0,
			comments: d[i].comments ? d[i].comments.summary.total_count : 0,
		})
	};
	return data;
}
// init();
module.exports = {
	get: init
}