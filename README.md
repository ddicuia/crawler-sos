#cheFare Analysis tool

Tools for analysis of che fare campaign.

It consists of
+	a chart crawled from cheFare pages
+	facebook data for all participants
+	history of votes crawled from cheFare pages

##Installation

Make sure you have node.js

in the main folder 

    npm install

Create a .env file at the root folder with your access token

    FACEBOOK_TOKEN = 'your token here'

##Start server

    node index.js





