var fs = require("fs");
var graph = require("./FacebookGraph.js");
var moment = require('moment-timezone');

var pagesArray = [{
	id: 431178446902266,
	name: "MyHomeGallery.org"
}, {
	id: 314623571908379,
	name: "We Are Cinema"
}, {
	id: 854506414639006,
	name: "La scuola open source"
}, {
	id: 293563447473567,
	name: "Tournée da Bar"
}];


function comment() {


	var fields = 'comments.summary(true).filter(stream).limit(1000){from}';

	var counter = 0;
	var commentData = [];
	getComments(pagesArray[counter]);

	function getComments(a) {
		var k = "/" + a.id + "/posts?limit=250&fields=" + fields;

		graph.get(k, function (err, r) {

			if (err) {
				console.log(err);
				return;
			}

			var array = [];

			for (var i = 0; i < r.data.length; i++) {

				var comments = r.data[i].comments.data;

				if (comments) {
					if (comments.length > 1) {
						for (var k = 0; k < comments.length; k++) {
							var name = comments[k].from.name;

							var found = false
							for (var z = 0; z < array.length; z++) {
								if (array[z].name == name) {
									array[z].rank++;
									found = true;
								}
							}

							if (!found) {
								array.push({
									name: name,
									rank: 0
								});
							}


						}
					}
				}
			}

			array.sort(function (a, b) {
				return b.rank - a.rank;
			});


			commentData.push({
				name: a.name,
				comments: array
			});

			if (counter < pagesArray.length - 1) {

				getComments(pagesArray[++counter])

			} else {

				fs.writeFile("commenti.json", JSON.stringify({
					data: commentData,
					date: moment.tz(new Date(), "Europe/Rome").format("hh:mma")
				}), "utf8", function () {
					console.log("commenti completato");
				});


			}
		});
	}


}

module.exports = {
	get: comment
}