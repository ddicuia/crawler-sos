var Crawler = require("crawler");
var fs = require("fs");
var moment = require('moment-timezone');


var urls = [
	"https://bando.che-fare.com/progetti-approvati/memoatlante/",
	"https://bando.che-fare.com/progetti-approvati/puglia-off/",
	"https://bando.che-fare.com/progetti-approvati/inside-out-community-theatre/",
	"https://bando.che-fare.com/progetti-approvati/la-piana-progetto-darte-partecipata/",
	"https://bando.che-fare.com/progetti-approvati/betwyll-social-reading-e-twitteratura-per-leditoria-listruzione-e-il-patrimonio-culturale/",
	"https://bando.che-fare.com/progetti-approvati/periferica-net/",
	"https://bando.che-fare.com/progetti-approvati/xanadu/",
	"https://bando.che-fare.com/progetti-approvati/teatroxcasa/",
	"https://bando.che-fare.com/progetti-approvati/articolo-27-cultura-bene-comune-per-linclusione-sociale/",
	"https://bando.che-fare.com/progetti-approvati/non-riservato/",
	"https://bando.che-fare.com/progetti-approvati/zigana/",
	"https://bando.che-fare.com/progetti-approvati/home-movies-digital-archive/",
	"https://bando.che-fare.com/progetti-approvati/piazza-di-brenta/",
	"https://bando.che-fare.com/progetti-approvati/creative-ground/",
	"https://bando.che-fare.com/progetti-approvati/ctrl-make-music-free/",
	"https://bando.che-fare.com/progetti-approvati/reti-culture-associazioni-italiachecipiace/",
	"https://bando.che-fare.com/progetti-approvati/liminaria/",
	"https://bando.che-fare.com/progetti-approvati/myhomegallery/",
	"https://bando.che-fare.com/progetti-approvati/la-scuola-open-source/",
	"https://bando.che-fare.com/progetti-approvati/we-are-cinema/",
	"https://bando.che-fare.com/progetti-approvati/officina-fundraising/",
	"https://bando.che-fare.com/progetti-approvati/cinedu-la-prima-piattaforma-di-cinema-per-la-didattica-interculturale/",
	"https://bando.che-fare.com/progetti-approvati/baumhaus/",
	"https://bando.che-fare.com/progetti-approvati/osi-orchestra-sociale-italiana/",
	"https://bando.che-fare.com/progetti-approvati/poetitaly-al-sud/",
	"https://bando.che-fare.com/progetti-approvati/assessorato-alle-piccole-cose/",
	"https://bando.che-fare.com/progetti-approvati/citta-tra-le-mani/",
	"https://bando.che-fare.com/progetti-approvati/c-ar-d-contemporary-art-design/",
	"https://bando.che-fare.com/progetti-approvati/3d-virtual-museum/",
	"https://bando.che-fare.com/progetti-approvati/interfacce-microeconomia-comunitaria-e-sostenibile-del-patrimonio-culturale/",
	"https://bando.che-fare.com/progetti-approvati/im-smart/",
	"https://bando.che-fare.com/progetti-approvati/streetart-factory/",
	"https://bando.che-fare.com/progetti-approvati/oilproject-una-scuola-gratis-online-per-tutti/",
	"https://bando.che-fare.com/progetti-approvati/tournee-da-bar/",
	"https://bando.che-fare.com/progetti-approvati/futura-scuola-community-di-cultura-dellintegrita-e-cittadinanza-monitorante/",
	"https://bando.che-fare.com/progetti-approvati/pigmenti-laboratorio-itinerante-di-arte-pubblica-e-narrazione/",
	"https://bando.che-fare.com/progetti-approvati/biblioteca-sociale-per-le-scuole/",
	"https://bando.che-fare.com/progetti-approvati/progettoborca/",
	"https://bando.che-fare.com/progetti-approvati/archeosharing-le-storie-di-tutti/",
	"https://bando.che-fare.com/progetti-approvati/italia-che-cambia/"
];


function go(done) {

	console.log("getting votes");
	var temp = [];
	var c = new Crawler({
		maxConnections: 10,
		// This will be called for each crawled page
		callback: function (error, result, $) {

			if (error) {
				console.log(error);
				return;
			}


			var titolo = $('#site_contentpadder > div:nth-child(2) > div.pad-top-20.pad-bottom-30 > h1').text();
			var voti = $("body > div.stripe.bg-blue.txt-white > div > div > div.col-half-intro-prj.alignleft.pad-top-20 > h2").text();
			var image = $("#site_contentpadder > div:nth-child(2) > div.pad-top-20.pad-bottom-30 > div.col-half.alignright > div.circle-100 > a > img").attr("src");
			var text = $("#site_contentpadder > div:nth-child(1) > div > div.col-post-title-intro.pad-top-30.meta-serif.txt-dark-grey > p > em").text();
			var link = $("#site_contentpadder > div:nth-child(2) > div.pad-top-20.pad-bottom-30 > div.col-half.alignright > div.circle-100 > a").attr("href");


			console.log(titolo, voti, image, text, link);

			temp.push({
				v: parseInt(voti.replace(" voti", "")),
				t: titolo,
				text: text,
				link: link,
				image: image,
				uri: result.uri
			});

		},
		onDrain: function () {

			//sort array by vote
			temp.sort(function (a, b) {
				// console.log(a);
				if (a.v > b.v) {
					return -1;
				}
				if (a.v < b.v) {
					return 1;
				}
				return 0;
			})

			//save votes
			fs.writeFile("voti.json", JSON.stringify({
				voti: temp,
				date: moment.tz(new Date(), "Europe/Rome").format("h:ma")
			}), "utf8", function () {
				console.log("voti completato");
				if (done) done();
			});


		}
	});
	c.queue(urls);
}

module.exports = {
	get: go
}