var fs = require("fs");

function get(done) {


	fs.readFile('./voti.json', 'utf8', function (err, data) {

		var lastVotes = JSON.parse(data).voti;
		var currentdate = new Date();

		fs.readFile('./public/storicovoti.json', 'utf8', function (err, data) {
			var storico;
			if (err) {
				// file doesn't exist
				storico = {};
				//create storico voti
				for (var i = 0; i < lastVotes.length; i++) {

					var name = lastVotes[i].t;

					storico[JSON.stringify(lastVotes[i].t)] = [{
						votes: lastVotes[i].v,
						date: currentdate
					}];


				}
			} else {
				storico = JSON.parse(data);
				for (var k = 0; k < lastVotes.length; k++) {
					var name = lastVotes[k].t;
					var o = storico[JSON.stringify(lastVotes[k].t)];
					if (o.length) {
						o.push({
							votes: lastVotes[k].v,
							date: currentdate
						});
					}
				}
			}


			fs.writeFile("./public/storicovoti.json", JSON.stringify(storico), "utf8", function () {
				console.log("storicovoti completato");
				done();
			});

		});
	});
}

module.exports = {
	get: get
}