	var nodeNum = 800;
	var minDist = 100;
	var springAmount = 0.0001;

	var nodes = [];
	var fontPoints = [];
	var canvas = document.getElementById("canvastext");
	var t = 0;

	canvas.width = 1000;
	canvas.height = 800;
	var ctx = canvas.getContext("2d");
	ctx.fillStyle = "black";
	ctx.fillRect(0, 0, 300, 300);
	ctx.font = "200px Lekton";
	ctx.fillStyle = '#ffffff';
	ctx.lineWidth = 3;
	ctx.fillText("SOS", 10, 300);
	for (var i = 0; i < 1000; i += 6) {
		for (var k = 0; k < 300; k += 6) {
			var c = ctx.getImageData(i, k, 1, 1).data;
			if (c[0] > 0 && c[1] > 0 && c[2] > 0) {
				fontPoints.push([i, k]);
			}
		}
	}
	var stage;
	var ballTexture;
	var renderer;
	var particleCont = new PIXI.ParticleContainer();
	var stageWidth
	var stageHeight
	var cols;
	var graph = new PIXI.Graphics();
	var button = new PIXI.Sprite.fromImage("assets/button.png");


	function init() {

		stageWidth = document.documentElement.clientWidth;
		stageHeight = document.documentElement.clientHeight;

		cols = Math.ceil(stageWidth / 100);
		stage = new PIXI.Stage(0xffffff, true);

		ballTexture = new PIXI.Texture.fromImage("assets/bubble_32x32.png");

		renderer = PIXI.autoDetectRenderer(stageWidth, stageHeight);
		renderer.view.style.display = "block";

		document.body.appendChild(renderer.view);
		createNodes();
		requestAnimationFrame(animate);
	}

	function createNodes() {

		for (var i = 0; i < nodeNum; i++) {
			var rnd = getRandomArbitrary(1, 5);
			var a = new Agent({
				x: getRandomArbitrary(0, stageWidth),
				y: getRandomArbitrary(0, stageHeight),
				vx: getRandomArbitrary(-100, 100) / 100,
				vy: getRandomArbitrary(-100, 100) / 100
			});

			a.alpha = .1;
			// particleCont.addChild(a);
			nodes.push(a);
		};

		for (var i = 0; i < fontPoints.length; i++) {
			var x = fontPoints[i][0] * 2.5 + Math.random() * 10 + stageWidth / 2 - 500;
			var y = fontPoints[i][1] * 2.5 + Math.random() * 10 - 250;
			var dx = stageWidth / 2 - x;
			var dy = stageHeight / 2 - y;
			var d = Math.sqrt(dx * dx + dy * dy);
			var a = new Agent({
				x: x,
				y: y,
				vx: 0,
				vy: 0,
				still: true,
				ampx: dx * Math.random(),
				ampy: dy * Math.random()
			});

			a.alpha = 0;
			// particleCont.addChild(a);
			nodes.push(a);
		};
		// stage.addChild(particleCont);
		button.position.x = stageWidth / 2;
		button.position.y = stageHeight / 2 + 200;
		button.anchor.x = .5;
		button.anchor.y = .5;
		button.scale.x = .5;
		button.scale.y = .5;

		stage.addChild(graph);
		// stage.addChild(button);

	}
	var sint;

	function animate() {
		t += 0.0005;
		sint = Math.sin(t);
		tiles = [];
		graph.clear();
		graph.moveTo(0, 0);
		graph.lineTo(200, 200);
		for (var i = 0; i < nodes.length; i++) {
			//update
			nodes[i].update();
		};

		renderer.render(stage);
		requestAnimationFrame(animate);
	}


	function getRandomArbitrary(min, max) {
		return Math.random() * (max - min) + min;
	}


	var Agent = function (o) {

		PIXI.Sprite.call(this, ballTexture)
		this.follow = Math.random() > .8;
		for (var i in o) {
			this[i] = o[i];
		}

		this.ox = this.x;
		this.oy = this.y;
		this.color = Math.random() * 0xffffff;
	}


	Agent.prototype = Object.create(PIXI.Sprite.prototype);
	Agent.prototype.constructor = Agent;

	Agent.prototype.update = function () {


		var tx = Math.floor(this.x / 100);
		var ty = Math.floor(this.y / 100);
		var index = ty * cols + tx;

		if (!tiles[index]) tiles[index] = [];

		if (!this.still) {


			this.x += this.vx;
			this.y += this.vy;


			if (this.x > stageWidth) this.vx *= -1;
			if (this.y > stageHeight) this.vy *= -1;
			if (this.x < 0) this.vx *= -1;
			if (this.y < 0) this.vy *= -1;

		} else {
			this.x = this.ox + Math.sin(sint * this.ampx) * 10;
			this.y = this.oy + Math.cos(sint * this.ampy) * 10 + this.ampy * .1;
			// Math.cos(t) * .0001;
			// this.y += Math.sin(t);
		}

		var t;
		var dx;
		var dy;
		var d;

		for (var i = 0; i < tiles[index].length; i++) {

			t = tiles[index][i];
			dx = this.x - t.x;
			dy = this.y - t.y;
			d = dx * dx + dy * dy;
			if (d < (this.still ? 1000 : 400)) {
				graph.lineStyle(1, this.still ? 0xffffff : 0xffffff, .2);
				graph.moveTo(this.x + 2, this.y + 2);
				graph.lineTo(t.x + 2, t.y + 2);
			}
		}


		tiles[index].push(this);
	}

	document.addEventListener("DOMContentLoaded", init)