var colorScale = d3.scale.category20();


var facebookData;
var votes;
var allNodes = [];
var viz = d3.select("#viz-cont");

var padding = 50;
var width = viz.node().getBoundingClientRect().width;
var height = 400;
var currentCircle;
var currentScale = 1;
var isLikescale = true;

// dom elements
var checkboxes;
var feeds;
var circles;
var checkboxesLabels;
/* localization */
moment.locale('it');


/* graph svg */
var svg = viz
	.append("svg")
	.attr("id", "viz")
	.attr("width", width)
	.attr("height", height);


var radio = d3.select(".radio")
	.html('<form><label><input type="radio" name="mode" value="likes" checked> Like</label><label><input type="radio" name="mode" value="shares"> Condivisioni</label></form>')

radio.selectAll("input").on("change", function () {
	isLikescale = (this.value === "likes");
	yAxis.scale(isLikescale ? likeScale : sharesScale);
	textLike.text(isLikescale ? "Like" : "Condivisioni");
	drawGraph(true);
});


/* tooltip */

var tooltip = d3.select("#tooltip").style("opacity", 0)

/* date */
var myTimeFormatter = function (date) {
	return moment(date).format("MMM DD");
}
var parseDate = d3.time.format.utc("%Y-%m-%dT%H:%M:%S%Z").parse;

/* Define Scales */
var xScale = d3.time.scale()
	.range([padding, width - padding * 2]);


var commentScale = d3.scale.linear().range([4, 15])
var sharesScale = d3.scale.linear().range([height - padding, 40])
var likeScale = d3.scale.linear().range([height - padding, 40]);
var voteScale = d3.scale.linear().range([height - padding, 40]);

var zoom = d3.behavior.zoom().scaleExtent([0.5, 20])
	.center([width / 2, height / 2])
	.on("zoom", function () {
		currentScale = d3.event.scale;
		drawGraph(true);
	});

var lineVoteFunction = d3.svg.line()
	.x(function (d) {
		return xScale(new Date(d.date));
	})
	.y(function (d, i, n) {
		return voteScale(d.votes);
	})
	.interpolate("linear");


//pane for zoom
var zoomPane = svg.append("svg:rect")
	.attr("class", "pane")
	.attr("width", width)
	.attr("height", height)
	.call(zoom);

var layersContainer = svg.append("g").attr("class", "layersContainer");

//left padding to hide graph
svg.append("rect")
	.attr("width", padding)
	.attr("fill", "black")
	.attr("height", height)
// .attr("transform", "translate(-" + (padding + 5) + ", 0)")

//right padding to hide graph
var rightPadding = svg.append("rect")
	.attr("width", padding)
	.attr("fill", "black")
	.attr("height", height)
	.attr("transform", "translate(" + (width - padding) + ", 0)")

var xAxis = d3.svg.axis()
	.scale(xScale)
	// .tickFormat(myTimeFormatter)
	.orient("bottom");

var xTicks = svg.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + (height - 25) + ")");

xTicks.call(xAxis);

var yAxis = d3.svg.axis()
	.scale(likeScale)
	.tickSize(0)
	// .tickSize(width - padding * 2)
	.orient("left");

var yTicks = svg.append("g")
	.attr("transform", "translate(" + (padding) + ", 0)")
	.attr("class", "y axis");

var voteAxis = d3.svg.axis()
	.scale(voteScale)
	.tickSize(0)
	.orient("right");

var voteTicks = svg.append("g")
	.attr("transform", "translate(" + (width - padding) + ", 0)")
	.attr("class", "y axis")
	.call(voteAxis);


var textLike = svg.append("g")
	.append("text")
	.attr("transform", "rotate(-90)")
	.attr("class", "label")
	.attr("x", -height + padding - 10)
	.attr("y", padding - 30)
	.text("Like");

var textVotes = svg.append("g")
	.append("text")
	.attr("transform", "rotate(-90)")
	.attr("class", "label")
	.attr("x", -height + padding - 10)
	.attr("y", width - 10)
	.text("Votes");


var loadingBar = new LoadingBar(svg);


loadingBar.load("facebook.json", onFacebookLoaded);


function onFacebookLoaded(json) {
	facebookData = json.data;
	loadingBar.load("/api/storico", onStoricoLoaded);

}

function onStoricoLoaded(data) {
	votes = data;
	createNodes();
}


function createNodes() {

	var newData = [];

	votes.forEach(function (d, i) {

		var name = d.name;
		var foundIndex = -1;

		facebookData.forEach(function (feed, i) {
			if (feed.name == name) {
				foundIndex = i;
				d.facebookFeed = feed;
			}
		});

		if (foundIndex != -1) {
			facebookData.splice(foundIndex, -1);
		}


	});

	console.log(votes)
	return;

	data.forEach(function (feed, i) {

		feed.active = false;
		feed.votes = votes[feed.name];

		if (feed.feed && feed.votes != undefined && feed.votes.length) {

			feed.totalcomments = 0;
			feed.totalshares = 0;
			feed.totallikes = 0;
			feed.nodes = [];
			feed.highlighted = false;
			feed.voteDomain = [feed.votes[0].votes, feed.votes[feed.votes.length - 1].votes]


			feed.feed.forEach(function (n, i) {

				n.name = feed.name;
				n.time = parseDate(n.created_time);
				n.feed = feed;
				n.link = getLinkFromID(n);

				feed.totalshares += n.shares;
				feed.totalcomments += n.comments;
				feed.totallikes += n.likes;
				//nodes that are comment to own nodes have no message AND the same id of the original post wtf... 
				if (n.message !== undefined) feed.nodes.push(n);
				/* allnodes is only used to calculate the axes domain, we could have done it better */
				allNodes.push(n);

			});

			feed.nodes.sort(function (a, b) {
				return a.time - b.time;
			})

			//  set neighbors for navigation 
			feed.nodes.forEach(function (d, i) {
				if (i > 0) d.next = feed.nodes[i - 1];
				if (i < feed.nodes.length - 1) d.previous = feed.nodes[i + 1];
			})

			newData.push(feed)
		} else {
			console.log("no data for " + feed.name);
		}

	});

	data = newData;


	data.sort(function (a, b) {
		return b.voteDomain[1] - a.voteDomain[1]
	});

	data.forEach(function (d, i) {
		console.log(d.voteDomain[1]);
		d.color = colorScale(i);
	})

	//activate first three
	for (var i = 0; i < 3; i++) {
		data[i].active = true;
	}

	setDomains();

	feeds = layersContainer
		.selectAll(".feed")
		.data(data)
		.enter()
		.append("g")
		.attr("class", "feed")


	circles = feeds
		.selectAll(".circle")
		.data(function (d) {
			return d.feed;
		})
		.enter()
		.append("circle")
		.attr("fill-opacity", 1)
		.style("cursor", "pointer")
		.style("fill", function (d) {
			return d.feed.color;
		})
		.attr("cy", function (d) {
			return isLikescale ? likeScale(d.likes) : sharesScale(d.shares);
		})
		.call(zoom)
		.on("mouseover", function (d) {
			d3.select(this.parentNode).selectAll("circle").each(function (d) {
				d.selected = false;
			});
			d.selected = true;
		})
		.on("mousedown", function (d) {
			showPost(d, this);
		})


	votes = feeds
		.append("path")
		.attr("stroke", function (d) {
			return d.color;
		})
		.attr("fill", "none")
		.attr("stroke-width", 3)

	xScale.domain(d3.extent(allNodes, function (d) {
		return d.time;
	}));


	// activate zoom
	zoom.x(xScale);


	drawLegend();
	drawGraph();

	window.onresize = onWindowresized;

}


function onWindowresized() {
	console.log("obj");
	var width = viz.node().getBoundingClientRect().width;
	svg.attr(width);
	xScale.range([padding, width - padding * 2]);
	zoom.center([width / 2, height / 2])
	zoomPane.attr("width", width)
	rightPadding.attr("transform", "translate(" + (width - padding) + ", 0)")
	voteTicks.attr("transform", "translate(" + (width - padding) + ", 0)")
	drawGraph();
}


function drawGraph(zooming) {


	if (zooming) {
		votes
			.attr("d", function (d) {
				return lineVoteFunction(d.votes || [])
			});
	} else {
		votes
			.transition()
			.attr("d", function (d) {
				return lineVoteFunction(d.votes || [])
			});
	}

	feeds
	// .transition(500)
	.attr("opacity", function (d) {
		return d.active ? (d.highlighted ? 1 : .5) : 0
	});

	circles
		.attr("cx", function (d) {
			return xScale(d.time);
		})

	circles
		.style("pointer-events", function (d) {
			return d.feed.active ? 'auto' : 'none'
		})
		.transition()
		.duration(500)
		.attr("cy", function (d) {
			return isLikescale ? likeScale(d.likes) : sharesScale(d.shares);
		})
		.attr("fill-opacity", function (d) {
			return d.selected ? 1 : .9
		})
		.attr("r", function (d) {
			return commentScale(d.comments);
		})


	// redraw axes

	yTicks
		.call(yAxis)

	xTicks
		.call(xAxis)

	voteTicks
		.call(voteAxis);


}


function drawLegend() {

	numeral.language('it', {
		delimiters: {
			thousands: '.',
			decimal: ','
		}
	});

	numeral.language('it');


	/* Legend */
	var legend = d3.select("#legend")


	var list = legend.selectAll('div')
		.data(data)
		.enter()
		.append('label')
		.attr('class', 'option')
		.style('cursor', 'pointer')
		.on('mouseup', checkboxClicked)


	list
		.append('input')
		.attr('type', 'checkbox')
		.attr('checked', function (d) {
			return d.active ? 'checked' : null;
		})

	list
		.append('span')
		.text(function (d) {
			return d.name.replace(/^(.{11}[^\s]*).*/, "$1");
		})
		.style("color", function (d) {
			return d.color
		})

	// checkboxes


	// checkboxes


}


function selectFeed(feed) {
	d3.selectAll(".riepilogo").each(function (d) {
		d.highlighted = (d == feed);
	});

	drawGraph();
}

function checkboxClicked(d) {


	var isChecked = !(d3.select(this).select('input').node().checked);

	data.forEach(function (k) {
		if (k.name === d.name) {
			k.active = isChecked;
			console.log(k.active);
		}
	})


	setDomains();
	drawGraph();
}

function setDomains() {

	var likes = [];
	var comments = [];
	var shares = [];
	var voteDomain = [Infinity, 0];
	data.forEach(function (feed) {

		if (feed.active) {
			feed.nodes.forEach(function (node) {
				likes.push(node.likes);
				comments.push(node.comments);
				shares.push(node.shares);
			});

			if (feed.voteDomain) {

				if (feed.voteDomain[0] < voteDomain[0]) voteDomain[0] = feed.voteDomain[0];
				if (feed.voteDomain[1] > voteDomain[1]) voteDomain[1] = feed.voteDomain[1];

			}
		}
	});

	commentScale.domain([d3.min(comments), d3.max(comments)])
	sharesScale.domain([d3.min(shares), d3.max(shares)])
	likeScale.domain([d3.min(likes), d3.max(likes)])
	voteScale.domain(voteDomain);


	if (isLikescale) {
		yAxis.scale(likeScale)
	} else {
		yAxis.scale(sharesScale)
	}


}

function showPost(d, node) {


	//hide others only do it if you're showing post from node
	if (node) {

		feeds.each(function (d) {
			d.highlighted = (this == node.parentNode);
		})

	}


	currentCircle = node;


	// node.transition().duration(200).attr("fill-opacity", 1);

	tooltip.style("opacity", 1)
		.transition()
		.duration(200)
	tooltip.html("<p style='color:" + d.feed.color + "'>" + d.name + "</p>" +
		"<p class='meta'><span>commenti:</span> " + nf(d.comments) + "</p>" +
		"<p class='meta'><span>condivisioni:</span> " + nf(d.shares) + "</p>" +
		"<p class='meta'><span>likes:</span> " + nf(d.likes) + "</p>" +
		"<div id='navigation'></div>" +
		((d.picture) ? "<img src='" + d.picture + "' />" : "") +
		"<p>" + d.message + "</p>" +
		"<p><a class='overlay' href='" + d.link + "' target='_blank'>Vai al post originale</a></p>" +
		"<p class='date'>postato il " + moment(d.created_time).format('LLL') + "</p>"
	);

	var nav = tooltip.select("#navigation");

	// TODO previous and next are inverted
	if (d.next && d.next.id) {
		nav.append("a").attr("href", "#").html("← precedente | ").on("mousedown", function (e) {
			showPost(d.next, svg.select("#id" + d.next.id));
		});
	}

	if (d.previous && d.previous.id) {
		nav.append("a").attr("href", "#").html("successivo →").on("mousedown", function (e) {
			showPost(d.previous, svg.select("#id" + d.previous.id));
		});
	}

	drawGraph();
}


/* helpers */
// utility for formatting numbers
function nf(n) {
	return numeral(n).format();
}

function getLinkFromID(n) {
	var a = n.id.split("_");
	var user = a[0];
	var post = a[1];
	return "http://www.facebook.com/" + user + "/posts/" + post;

}