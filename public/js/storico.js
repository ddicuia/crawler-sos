// largely based on http://bl.ocks.org/WillTurman/4631136 
var colors = d3.scale.category20()


var format = d3.time.format("%m/%d/%y");

var margin = {
	top: 50,
	right: 50,
	bottom: 50,
	left: 50
};

var width = document.body.querySelector('.chart').offsetWidth - margin.left - margin.right;
var height = 500 - margin.top - margin.bottom;

var vertical;
var layersContainer;

var giornaliero = false;
var hourIncrement = 24;

// data from Json
var jsonData;
//contains displayed data
var currentData = [];

var layers;

var isSingle = false;

var x = d3.time.scale()
	.range([0, width]);

var y = d3.scale.linear()
	.range([height - 10, 0]);


var zoom = d3.behavior.zoom()
	.center([width / 2, height / 2])
	.scaleExtent([1, 20])
	.on("zoom", function () {
		var wait = false;
		return function () {
			drawLayers(false);

			//throttle savehistory
			if (!wait) {
				wait = true;
				// saveHistory();
				setTimeout(function () {
					wait = false;
				}, 1000);
			}
		}
	}());


var xAxis = d3.svg.axis()
	.scale(x)

var yAxis = d3.svg.axis()
	.scale(y);

var yAxisr = d3.svg.axis()
	.scale(y);

var stack = d3.layout.stack()
	.offset("silhouette")
	.order("reverse")
	.values(function (d) {
		return d.growthValues;
	})

var lineVoteFunction = d3.svg.line()
	.x(function (d) {
		return x(d.x);
	})
	.y(function (d, i, n) {
		return y(d.y);
	})
	.interpolate("cardinal");


var area = d3.svg.area()
	.interpolate("cardinal")
	.x(function (d) {
		return x(d.x);
	})
	.y0(function (d) {
		return y(d.y0);
	})
	.y1(function (d, i) {
		return y(d.y0 + d.y);
	});

var zeroArea = d3.svg.area()
	.interpolate("linear")
	.x(function (d) {
		return x(d.x);
	})
	.y0(function (d) {
		return height / 2;
	})
	.y1(function (d, i) {
		return height / 2;
	});

// ----------------- DOM ----------------------------------------------------------------

var checkboxes;
var labels;

var tooltip = d3.select(".chart")
	.append("div")
	.attr("id", "tooltip")
	.attr("class", "remove")
	.style("position", "absolute	")
	.style("z-index", "20")
	.style("visibility", "hidden")
	.style("top", "60px")
	.style("left", (margin.left + 10) + "px");


var inputLabel = d3.select(".container .right")
	.append("label")
	.text("Intervallo di tempo")

var input = d3.select(".container .right").append("input");

input.attr("type", "range")
	.attr("value", "24")
	.attr("min", "1")
	.attr("max", "24")
	.on("input", function () {
		hourIncrement = +this.value;
		resetData();
		inputLabel.text(this.value + "h")

		drawLayers(false);
	});


var svg = d3.select(".chart").append("svg")
	.attr("width", width + margin.left + margin.right)
	.attr("height", height + margin.top + margin.bottom)
	.append("g")
	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");


var legend = d3.select(".legend");


var loadingBar = new LoadingBar(svg);
loadingBar.load("/api/storico", init);
// loadingBar.load("/storicovoti2.json", init);


function init(d) {

	jsonData = d;

	jsonData.sort(function (a, b) {
		var maxA = d3.max(a.votes, function (d) {
			return d.votes;
		});
		var maxB = d3.max(b.votes, function (d) {
			return d.votes;
		});
		return maxB - maxA
	});


	colors.domain(d3.range(10))
	var colorrange = colors.range();

	var z = d3.scale.ordinal().range(colorrange);
	//assign unique colors
	for (var i = 0; i < jsonData.length; i++) {
		jsonData[i].color = z(i);
	}

	resetData();

	var firstFeedValues = currentData[0].values;
	x.domain([new Date(firstFeedValues[0].x), new Date(firstFeedValues[firstFeedValues.length - 1].x)]);


	for (var i = 0; i < 10; i++) {
		currentData[i].active = true;
	};


	d3.select(".selezionatutti")
		.on("mousedown", function (e) {

			d3.event.stopImmediatePropagation();
			d3.event.stopPropagation();
			d3.event.preventDefault();


			for (var i = 0; i < currentData.length; i++) {
				currentData[i].active = true;
			}

			drawLayers();

		})

	d3.select(".rimuovitutti")
		.on("mousedown", function (e) {
			d3.event.stopImmediatePropagation();
			d3.event.stopPropagation();
			d3.event.preventDefault();

			for (var i = 0; i < currentData.length; i++) {
				currentData[i].active = false;
			}

			currentData[0].active = true;

			drawLayers();

		})

	var list = legend
		.selectAll('div')
		.data(currentData)
		.enter()
		.append('div')
		.attr("class", "element")

	labels = list
		.data(currentData)
		.append('label')


	checkboxes = labels
		.data(currentData)
		.insert('input', ':first-child')
		.attr('type', 'checkbox')
		.attr('checked', "checked")
		.on('change', checkBoxClicked);


	labels.data(currentData)
		.append('p')
		.text(function (d) {
			return d.key.replace(/\"/g, "");
		})

	//////// INIT GRAPH


	svg.append("svg:rect")
		.attr("class", "pane")
		.attr("width", width)
		.attr("height", height)
		.call(zoom);

	layersContainer = svg.append("g");


	//left padding to hide graph
	svg.append("rect")
		.attr("width", margin.left)
		.attr("fill", "black")
		.attr("height", height)
		.attr("transform", "translate(-" + (margin.left + 5) + ", 0)")

	//right padding to hide graph
	svg.append("rect")
		.attr("width", margin.right)
		.attr("fill", "black")
		.attr("height", height)
		.attr("transform", "translate(" + (width + 5) + ", 0)")


	svg.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0, " + height + ")")
		.call(xAxis.orient("bottom"));


	svg.append("g")
		.attr("class", "y axis right")
		.attr("transform", "translate(" + (width + 5) + ", 0)")
		.call(yAxis.orient("right"));

	svg.append("g")
		.attr("class", "y axis left")
		.attr("transform", "translate(" + (-5) + ", 0)")
		.call(yAxis.orient("left"));


	// vertical = d3.select(".chart")
	// 	.style("position", "relative")
	// 	.append("div")
	// 	.attr("class", "remove")
	// 	.attr("transform", "translate(0," + margin.top + ")")
	// 	.style("position", "absolute")
	// 	.style("z-index", "19")
	// 	.style("width", "1px")
	// 	.style("height", (height - 10) + "px")
	// 	.style("top", "50px")
	// 	.style("left", "0px")
	// 	.style("background", "#fff");

	d3.select(".chart")
		.on("mousemove", function () {
			mousex = d3.mouse(this);
			mousex = mousex[0] + 5;
			// vertical.style("left", mousex + "px")
		})
		.on("mouseover", function () {
			mousex = d3.mouse(this);
			mousex = mousex[0] + 5;
			// vertical.style("left", mousex + "px")
		});

	zoom.x(x)

	// getCurrentState();
	// checkBoxClicked();
	drawLayers();

}


function resetData() {

	var counter = 0;
	var maxDate;
	var minDate;

	var d = jsonData;
	currentData = [];


	var activeCheckboxes = [];
	if (checkboxes) {
		checkboxes.each(function (d) {
			if (d.active) {
				activeCheckboxes.push(d.key);
			}
		});
	}
	for (var i = 0; i < d.length; i++) {

		var values = [];
		var growthValues = [];

		var date;
		var prevVotes = 0;

		// create data points
		for (var k = 0; k < d[i].votes.length; k += hourIncrement) {

			var o = d[i].votes[k];
			date = new Date(o.date);


			values.push({
				x: date,
				y: o.votes
			});

			var pp = (o.votes - prevVotes);
			growthValues.push({
				x: date,
				y: !k ? 0 : pp
			});

			prevVotes = o.votes;


		}

		// return;
		currentData.push({
			// limit to 11 chars 
			// key: d[i].name.replace(/^(.{11}[^\s]*).*/, "$1"),
			key: d[i].name,
			values: values,
			index: counter,
			active: activeCheckboxes.indexOf(d[i].name) != -1,
			growthValues: growthValues,
			color: d[i].color
		});


		counter++;

	}


}
// get the graph state from the URL 
// function getCurrentState() {
// 	var currentState = window.location.hash.split("#");

// 	if (currentState.length > 1) {
// 		var currentTransation = currentState[1].substring(2).split(",")
// 		var currentScale = currentState[2].substring(2)
// 		var mode = currentState[3].substring(2);
// 		var indices = currentState[4].substring(2).split(",")
// 		indices.forEach(function (d, i) {
// 			indices[i] = parseInt(d)
// 		});

// 		if (mode && currentTransation && currentScale) {
// 			if (indices) {

// 				currentData = [];
// 				currentindices = [];
// 				for (var i = 0; i < indices.length; i++) {
// 					currentData.push(data[indices[i]]);
// 					currentindices.push(indices[i]);
// 				}

// 			}

// 			votiMode = (mode == "0");
// 			changeMode();

// 			updateCheckBox()

// 			zoom.translate([parseFloat(currentTransation[0]), parseFloat(currentTransation[1])]);
// 			zoom.scale(currentScale);


// 		}
// 	}


// }

function updateDomain(d) {
	y.domain([0, d3.max(d, function (d) {
		return d3.max(d.growthValues, function (d, i) {
			console.log(d.y0 + d.y);
			return d.y0 + d.y;
		})
	})]);

}


function drawLayers(isAnimate) {


	var myLayers = stack(currentData.filter(function (a) {
		return a.active;
	}));

	updateDomain(myLayers);


	checkboxes
		.data(currentData)
		.attr("checked", function (d) {
			return d.active ? "checked" : null
		})

	labels
		.data(currentData)
		.select("p")
		.style("color", function (d) {
			return d.active ? d.color : "#494949"
		})

	// TODO why traversing the DOM?
	// re adjust axes
	svg.selectAll(".y.axis.left")
		.call(yAxis.orient("left"));

	svg.selectAll(".y.axis.right")
		.call(yAxis.orient("right"));

	svg.selectAll(".x.axis")
		.call(xAxis.orient("bottom"));


	svg.selectAll(".layer")
		.data(myLayers)
		.exit()
		.remove()


	if (false) {

		//visualize line
		layersContainer.selectAll(".layer")
			.data(myLayers)
			.enter()
			.append("path")
			.attr("class", "layer")
			.style("fill-opacity", 0)
			.style("stroke", function (d, i) {
				return d.color;
			})


		layersContainer.selectAll(".layer")
			.transition()
			.duration(function () {
				return !isAnimate ? 0 : 250
			})
			.attr("d", function (d) {
				return lineVoteFunction(d.growthValues); //votiMode ? d.values : d.growthValues);
			})

	}

	// append
	layersContainer.selectAll(".layer")
		.data(myLayers)
		.enter()
		.append("path")
		.style("cursor", "pointer")
		.attr("class", "layer")
		.on("mouseover", onMouseOver)
		.on("mousemove", onMouseMove)
		.on("mouseout", onMouseOut)
		.on("mousedown", onMouseDown)
		.call(zoom);


	//update

	layersContainer.selectAll(".layer")
		.style("fill", function (d, i) {
			return d.color;
		})
		.transition()
		.duration(function () {
			return !isAnimate ? 0 : 250
		})
		.attr("d", function (d) {
			return area(d.growthValues); //votiMode ? d.values : d.growthValues);
		})


	// prevMode = votiMode

}

function onMouseDown(d, i) {
	d.active = false;
	drawLayers(true);
}


function onMouseMove(d, i) {

	// if (isSingle && d3.select(this) != isSingle) return;

	d3.select(this)
		.classed("hover", true)
		.attr("stroke", "#fff")
		.attr("stroke-width", "1px");


	var mousex = d3.mouse(this);
	var mousex = mousex[0];
	var invertedx = x.invert(mousex);

	if (invertedx) {

		var selected = (d.values);
		var closestDate = Infinity;
		var dateIndex = -1;
		var datearray = [];

		for (var k = 0; k < selected.length; k++) {
			var dx = Math.abs(selected[k].x - invertedx);
			if (dx < closestDate) {
				closestDate = dx;
				dateIndex = k;
			}
		}

	}
	pro = "+" + (d.values[dateIndex].y - d.values[dateIndex - 1].y);
	// pro = votiMode ? d.values[dateIndex].y : ("+" + (d.values[dateIndex].y - d.values[dateIndex - 1].y));
	var dateString = moment(d.values[dateIndex].x).format('ddd DD/MM, h:mma');
	tooltip.html("<p>" + d.key.replace(/\"/g, "") + "<br>" + dateString + "<br>" + pro + " voti</p>").style("visibility", "visible");
}

function onMouseOut(d, i) {
	// vertical.style("opacity", 0)
	// if (isSingle) return;

	svg.selectAll(".layer")
		.transition()
		.duration(250)
		.attr("opacity", "1");

	d3.select(this)
		.classed("hover", false)
		.attr("stroke-width", "0px"), tooltip.html("<p>" + d.key).style("visibility", "hidden");
}

function onMouseOver(d, i) {
	// vertical.style("opacity", 1)

	// if (isSingle) return;

	svg.selectAll(".layer")
		.transition()
		.duration(250)
		.attr("opacity", function (d, j) {
			return j != i ? 0.6 : 1;
		});

}


function checkBoxClicked(d) {


	d.active = this.checked;

	drawLayers(true)

}


function saveHistory() {

	var ind = "";
	for (var i = 0; i < currentindices.length; i++) {
		ind += currentindices[i];
		if (i < currentindices.length - 1) ind += ","
	};

	window.history.pushState("tra", "asdf", "#t=" + zoom.translate() + "#s=" + zoom.scale() + "#m=" + (votiMode ? 0 : 1) + "#i=" + ind);

}

function changeMode() {
	// stack.offset("silhouette")
}

// used for debug purposes
function stream_layers(n, m, o) {
	if (arguments.length < 3) o = 0;

	function bump(a) {
		var x = 1 / (.1 + Math.random()),
			y = 2 * Math.random() - .5,
			z = 10 / (.1 + Math.random());
		for (var i = 0; i < m; i++) {
			var w = (i / m - y) * z;
			a[i] += x * Math.exp(-w * w);
		}
	}
	return d3.range(n).map(function () {
		var a = [],
			i;
		for (i = 0; i < m; i++) a[i] = o + o * Math.random();
		for (i = 0; i < 5; i++) bump(a);

		return {
			key: "name" + Math.random() * 10,
			values: a.map(stream_index)
		};
	});


	function stream_index(d, i) {
		var a = new Date();
		a.setHours(i);

		return {
			x: a,
			y: d * 900
		};
	}

}

function shuffle(array) {
	for (var i = array.length - 1; i > 0; i--) {
		var j = Math.floor(Math.random() * (i + 1));
		var temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	return array;
}