// shamelessy copied from http://bl.ocks.org/mbostock/3750941

var LoadingBar = (function (el) {

	var twoPi = 2 * Math.PI;
	var progress = 0;
	var total = 0;
	var formatPercent = d3.format(".0%");

	var arc = d3.svg.arc()
		.startAngle(0)
		.innerRadius(70)
		.outerRadius(100);

	var svg = el.append("svg")
		.attr("width", width)
		.attr("height", height)
		.append("g")
		.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

	var meter = svg.append("g")
		.attr("class", "progress-meter");

	meter.append("path")
		.attr("fill", "#333")
		.attr("d", arc.endAngle(twoPi));

	var foreground = meter.append("path")
		.attr("fill", "#bbb")
		.attr("class", "foreground");

	var text = meter.append("text")
		.attr("text-anchor", "middle")
		.attr("dy", ".35em");


	return {
		load: function (dataURL, callback) {

			meter.attr("transform", "scale(1)");
			foreground.attr("d", arc.endAngle(0));
			total = null;
			var isFinished = false

			d3.json(dataURL)
				.on("progress", function (a) {
					if (!total) total = a.getResponseHeader('Content-Length');
					var i = d3.interpolate(progress, d3.event.loaded / total);
					d3.transition().tween("progress", function () {
						return function (t) {
							progress = i(t);
							foreground.attr("d", arc.endAngle(twoPi * progress));
							text.text(formatPercent(progress));
						};
					});
				})
				.get(function (error, data) {
					meter.transition().delay(250).attr("transform", "scale(0)").each("end", function () {
						if (isFinished) return;
						isFinished = true;
						callback(data);
					});
				});
		}
	}
});