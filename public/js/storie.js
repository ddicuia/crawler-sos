var boxes;
var columns = 3;

var boxesToSort = [];
var allboxes = [];
var auth;


// facebook
window.fbAsyncInit = function () {
	FB.init({
		appId: '920968241317493',
		xfbml: true,
		version: 'v2.5'
	});
};
window.onload = function() {
	init();
}

(function (d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {
		return;
	}
	js = d.createElement(s);
	js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


function statusChangeCallback(response) {
	console.log('statusChangeCallback');
	console.log(response);
	// The response object is returned with a status field that lets the
	// app know the current login status of the person.
	// Full docs on the response object can be found in the documentation
	// for FB.getLoginStatus().
	if (response.status === 'connected') {
		// Logged into your app and Facebook.
		// testAPI();
		console.log("connected");
	} else if (response.status === 'not_authorized') {
		// The person is logged into Facebook, but not your app.
		console.log('Please log ' + 'into this app.');
	} else {
		// The person is not logged into Facebook, so we're not sure if
		// they are logged into this app or not.
		console.log('Please log ' + 'into Facebook.');
	}
}


function init() {
	boxes = document.querySelectorAll('.box-storia');
	for (var i = boxes.length-1; i >= 0 ; i--) {
		var titolo = document.createElement("h3");
		titolo.className = "titolo";
		titolo.textContent = "#STORIA N." + (i + 1);
		var head = boxes[i].querySelector(".header");
		head.insertBefore(titolo, head.firstChild);
		// var button = document.createElement("button");
		// button.textContent = "condividi"
		// button.className = "share-btn";

		// button.onmousedown = (function (e) {
		// 	var box = boxes[i];

		// 	return function () {
		// 		FB.login(function () {
		// 			auth = FB.getAuthResponse();
		// 			makeImage(box);

		// 		}, {
		// 			scope: 'publish_actions'
		// 		});

		// 	}
		// })();

		// boxes[i].appendChild(button);
		boxesToSort.push(boxes[i]);
	};

	// for (var i = boxesToSort.length - 1; i > 0; i--) {
	// 	var j = Math.floor(Math.random() * (i + 1));
	// 	var temp = boxesToSort[i];
	// 	boxesToSort[i] = boxesToSort[j];
	// 	boxesToSort[j] = temp;
	// }

	var partecipa = document.querySelector('.box-cta');

	var container = document.querySelector('.container');
	container.innerHTML = "";


	for (var i = boxesToSort.length - 1, k = 0; i >= 0; i--, k++) {
		var b = boxesToSort[i];
		container.appendChild(b);
		if (k % 5 == 0) {
			var bcta = partecipa.cloneNode(true);
			container.appendChild(bcta);
			allboxes.push(bcta);
		}
		allboxes.push(b);
	}


	var msnry = new Masonry(container, {
		// options
		itemSelector: '.item',
		columnWidth: '.item',
		percentPosition: true,
		gutter: 20

	});


	show();
}

window.onresize = function () {
	if(msnry) msnry.layout();
}

var lastScrolled = new Date();
window.onscroll = function () {
	if (new Date() - lastScrolled > 400) {
		lastScrolled = new Date();
		show();
	}
}


function show() {

	for (var i = 0; i < allboxes.length; i++) {
		var b = allboxes[i];
		if (elementInViewport(b) && !b.classList.contains('showing')) {
			b.classList.add('showing');
		}
	}

}


function elementInViewport(el) {
	var top = el.getBoundingClientRect().top;

	return (
		top < window.innerHeight * .9
		// (top + height) > window.pageYOffset
		);
}


function makeImage(box) {

	var big = $(".box-storia-big");
	var b = $(box).clone(true);
	big.append(b.children());

	big.append("<div class='sostieni'><p> #sos</p></div>")
	$("button", big).remove();

	html2canvas(big[0], {
		background: "#000"
	}).then(function (canvas) {
		var blob;
		try {
			blob = dataURItoBlob(canvas.toDataURL());
		} catch (e) {
			console.log(e);
		}
		big.remove();
		var fd = new FormData();
		fd.append("source", blob);
		fd.append("message", "Storie di #sos http://lascuolaopensource.xyz –Sostieni la scuola open source  – http://bit.do/votasos");

		try {
			$.ajax({
				url: "https://graph.facebook.com/me/photos?access_token=" + auth.accessToken,
				type: "POST",
				data: fd,
				processData: false,
				contentType: false,
				cache: false,
				success: function (data) {
					console.log("success " + data);
				},
				error: function (shr, status, data) {
					console.log("error " + data + " Status " + shr.status);
				},
				complete: function () {
					console.log("Posted to facebook");
				}
			});
		} catch (e) {
			console.log(e);
		}

	})
}

function dataURItoBlob(dataURI) {
	var byteString = atob(dataURI.split(',')[1]);
	var ab = new ArrayBuffer(byteString.length);
	var ia = new Uint8Array(ab);
	for (var i = 0; i < byteString.length; i++) {
		ia[i] = byteString.charCodeAt(i);
	}
	return new Blob([ab], {
		type: 'image/png'
	});
}